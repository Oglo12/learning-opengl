#!/usr/bin/env bash

[[ -d ./src ]] || echo "Please run in the project root!"
[[ -d ./src ]] || exit 1

[[ -d ./build-out ]] && rm -rf ./build-out

meson setup build-out

cd build-out

meson compile
