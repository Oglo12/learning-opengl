#!/usr/bin/env bash

export BINARY_NAME="learning-opengl"

[[ -d ./src ]] || echo "Please run in the project root!"
[[ -d ./src ]] || exit 1

./build.sh && echo "" && ./build-out/$BINARY_NAME
