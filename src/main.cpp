#include "renderer.h"
#include "vertex_buffer.h"
#include "index_buffer.h"

#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>
#include <sstream>

const std::string LINE = "===================================================================";

struct ShaderProgramSource {
    std::string vertexSource;
    std::string fragmentSource;
};

static ShaderProgramSource parseShader(const std::string &filepath) {
    std::ifstream stream(filepath);

    enum class ShaderType {
        NONE = -1,
        VERTEX = 0,
        FRAGMENT = 1,
    };

    std::string line;
    std::stringstream ss[2];
    ShaderType type = ShaderType::NONE;
    while(getline(stream, line)) {
        if (line.find("#shader") != std::string::npos) {
            if (line.find("vertex") != std::string::npos) {
                type = ShaderType::VERTEX;
            }

            else if (line.find("fragment") != std::string::npos) {
                type = ShaderType::FRAGMENT;
            }
        }

        else {
            ss[(int) type] << line << '\n';
        }
    }

    return {
        ss[0].str(),
        ss[1].str(),
    };
}

static int compileShader(unsigned int type, const std::string &source) {
    unsigned int id = glCreateShader(type);

    const char* src = source.c_str(); // source.c_str() is pretty much the same as &source[0] (address of the first character in the string)

    glShaderSource(id, 1, &src, nullptr);
    glCompileShader(id);

    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);

        char* message = (char*) alloca(length * sizeof(char));
        glGetShaderInfoLog(id, length, &length, message);

        std::cerr << "Failed to compile shader! (TYPE: " << (type == GL_VERTEX_SHADER ? "VERTEX" : "FRAGMENT") << ") Reason:\n";
        std::cerr << message << "\n";

        glDeleteShader(id);

        return 0;
    }

    return id;
}

static unsigned int createShader(const std::string &vertexShader, const std::string &fragmentShader) {
    unsigned int program = glCreateProgram();

    unsigned int vs = compileShader(GL_VERTEX_SHADER, vertexShader);
    unsigned int fs = compileShader(GL_FRAGMENT_SHADER, fragmentShader);

    glAttachShader(program, vs);
    glAttachShader(program, fs);

    glLinkProgram(program);

    glValidateProgram(program);

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
}

int main() {
    std::cout << "Hello, world! Let's learn some OpenGL! Ready? Let's go!\n\n";

    GLFWwindow* window;

    if (!glfwInit()) {
        std::cerr << "Failed to init GLFW!\n";
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // Use OpenGL version 4.
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4); // Use OpenGL version 4.
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Force the use of core profile.

    window = glfwCreateWindow(640, 480, "Learning OpenGL", NULL, NULL);

    if (!window) {
        std::cerr << "Failed to create window with GLFW!\n";
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    glfwSwapInterval(1); // Sync with monitor refresh rate.

    // Has to come after glfwMakeContextCurrent()!
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to init GLEW!\n";
        return -1;
    }

    std::cout << LINE << "\n";
    std::cout << "  OpenGL Version: " << glGetString(GL_VERSION) << "\n";
    std::cout << LINE << "\n";

    {
        // Vertex positions.
        float positions[] = {
            -0.5, -0.5,
             0.5, -0.5,
             0.5,  0.5,
            -0.5,  0.5,
        };

        // Indices. (Has to be unsigned!)
        unsigned int indices[] = {
            0, 1, 2,
            2, 3, 0,
        };
        unsigned int indicesLen = sizeof(indices) / sizeof(indices[0]);

        // Create the vertext array object.
        unsigned int vao;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        // Create the vertex buffer object.
        VertexBuffer vbo(positions, sizeof(positions));

        // glVertexAttribPointer(): // Set the layout of the bound vertex array object (VAO).
        //   1. index: Where to start.
        //   2. size: How many items in a vertex.
        //   3. type: Type of the data.
        //   4. normalized: Specifies whether fixed-point data should be normalized.
        //   5. stride: How big is a vertex? This is used as an offset to calculate where each vertex is.
        //   6. pointer: Where in the vertex is the position attribute? (Example: 0 if the position attribute is the first attribute.)
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0); // This is the code linking the bound VBO to the bound VAO.
        glEnableVertexAttribArray(0); // Enable index 0 of the VAO.

        // Create the index buffer object.
        IndexBuffer ibo(indices, indicesLen);

        ShaderProgramSource source = parseShader("res/shaders/basic.glsl");
        unsigned int shader = createShader(source.vertexSource, source.fragmentSource);
        glUseProgram(shader);

        // If location is assigned to -1, that means OpenGL couldn't find the uniform. (I am not going to check that, because I know this will work.)
        // This may not be an actual error though. Because if you have the uniform in your shader, but you never use it, OpenGL will strip it out at shader-compile-time.
        // Also, this uniform code must come after glUseProgram(), because you need a shader bound so OpenGL knows what shader to target.
        int location = glGetUniformLocation(shader, "u_Color");
        glUniform4f(location, 1.0, 0.0, 0.0, 1.0); // Set the uniform data. (Setting the value for: 'u_Color')
        int u_color_location = location;

        float r = 0.0;
        float g = 0.6;
        float b = 0.5;

        const float rgb_inc_amount = 0.01;
        float rgb_inc = rgb_inc_amount;

        // Application loop.
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT);

            glUniform4f(u_color_location, r, g, b, 1.0);

            // If you use GL_INT instead of GL_UNSIGNED_INT, you get a black screen.
            glDrawElements(GL_TRIANGLES, indicesLen, GL_UNSIGNED_INT, nullptr); // The indices field is a nullptr because the IBO is already bound.

            if (r > 0.4) {
                rgb_inc = rgb_inc_amount * -1.0;
            }

            else if (r < 0.0) {
                rgb_inc = rgb_inc_amount;
            }

            r += rgb_inc;

            glfwSwapBuffers(window);

            glfwPollEvents();
        }

        // Get ready to fully quit the program.
        glDeleteProgram(shader);
    }

    // Get ready to fully quit the program.
    glfwTerminate();

    return 0;
}
